PKGNAME := rockyou2021
PREFIX := /usr

prepare: clean
	git lfs fetch origin main
	gzip -c -d ${PKGNAME}.txt.gz > ${PKGNAME}.txt

install:
	install -dm755 $(PREFIX)/share/wordlists
	install -Dm644 ${PKGNAME}.txt $(PREFIX)/share/wordlists/${PKGNAME}.txt

clean:
	rm ${PKGNAME}.txt
